# Cats_vs_Dogs_Classification

Name : Shivam Raval

Objective : To build a classification model with Deep Residual Networks containing 50 layers (Resnet - 50) to classify dogs and cats from the images.

Approach:
Classification using Resnet-50 architecture. Basically Resnet-50 is a Deep Residual Network containing 50 layers. The whole architecture can be seen here (https://www.kaggle.com/keras/resnet50) . It contains similar layers like other deep networks but Resnet uses a residual learning framework. A residual learning framework could ease the training of networks that are substantially deeper than those used previously. ResNet explicitly reformulates the layers as learning residual functions with reference to the layer inputs, instead of learning unreferenced functions (https://arxiv.org/pdf/1512.03385v1.pdf). 


Libraries used : Keras 1.2 , Tensorflow (as keras use tensorflow as backend) , Numpy, OpenCV, os

Codes:

split_dataset.py : This script generates the training and testing dataset it transfers 1000 images of cats and 1000 of dogs to the testing dataset. So in total there are 23000 images for training and 2000 images for testing.

model.py : I have used transfer learning as it gains improvement of learning in a new task through the transfer of knowledge from a related task that has already been learned. I use keras to build the structure of ResNet-50 for Cats.Vs.Dogs classification. The weights of ResNet-50 for Cats.Vs.Dogs. without top layer were downloaded from keras and trained by ImageNet. The weights of top layer were trained by the dataset downloaded. The train and test dataset is generated using Image genetator available in keras. (https://github.com/keras-team/keras/issues/5862 ).  

As the goal was to get an accuracy above 90% and the model to use minimum disk space, transfer learning is the best approach which uses less memory and complexity. Without using transfer learning the complexity to train whole Resnet-50 is too high. The training is accuracy is 95% and the validation accuracy is 98%.






